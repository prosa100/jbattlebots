package pr.lib;

/**
 * A Vector 2
 *
 * @author Paul
 */
public class Vec {

    public final static Vec Right = new Vec(1, 0);
    public final static Vec Up = new Vec(0, -1);
    public final static Vec Left = new Vec(-1, 0);
    public final static Vec Down = new Vec(0, 1);
    public final static Vec North = Up, South = Down, East = Right, West = Left;
    public final static Vec Zero = new Vec(0, 0);
    public final int x;
    public final int y;

    public Vec(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vec plus(Vec o) {
        return new Vec(x + o.x, y + o.y);
    }

    public Vec times(int s) {
        return new Vec(x*s, y*s);
    }
    
    protected Vec v(int x, int y) {
        return new Vec(x, y);
    }

    public Vec minus(Vec o) {
        return new Vec(x - o.x, y - o.y);
    }

    public Vec InDirection(Dir d) {
        return this.plus(d.Forward);
    }
    
     public Vec InDirection(Dir d, int s) {
        return this.plus(d.Forward.times(s));
    }

    /*
     * The Distance without the sqrt.
     */
    public double mag2() {
        return x * x + y * y;
    }

    public double magnitude() {
        return Math.sqrt(mag2());
    }

    public double distanceTo(Vec o) {
        return this.minus(o).magnitude();
    }

    @Override
    public int hashCode() {
        final int prime = 211;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Vec other = (Vec) obj;
        if (x != other.x) {
            return false;
        }
        if (y != other.y) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[" + x + "," + y + "]";
    }
    
    public Vec[] getNeighbors() {
        Vec[] n = new Vec[4];
        for (int i = 0; i < 4; i++) {
            n[i] = this.InDirection(Dir.dirs[i]);
        }
        return n;
    }
}
