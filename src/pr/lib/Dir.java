package pr.lib;

/**
 * Stores a Direction
 *
 * @author Paul
 */
public enum Dir {

    None(-1, Vec.Zero),
    South(0, Vec.South),
    West(1, Vec.West),
    North(2, Vec.North),
    East(3, Vec.East);
    public Dir Left;
    public Dir Right;
    public Dir Back;
    public final Vec Forward;
    public final int value;
    public static final Dir[] dirs = {South, West, North, East};
    
    /*
     * A list with all of the Directions becuse I like Dir.s
     */
    public static final Dir[] s = dirs;
    
    Dir(int v, Vec forward) {
        this.value = v;
        this.Forward = forward;
    }

    public Dir turn(Turn turn) {
        if (this == None) {
            return None;
        }
        
        return dirs[(this.value + turn.value) % 4];
    }

    static {
        for (Dir d : dirs) {
            d.Right = d.turn(Turn.Right);
            d.Back = d.turn(Turn.Back);
            d.Left = d.turn(Turn.Left);
        }
    }
}
