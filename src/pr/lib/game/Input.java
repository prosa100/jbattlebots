package pr.lib.game;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Hashtable;

import javax.swing.KeyStroke;

public class Input implements KeyEventDispatcher {
	public static final boolean Down = true;
	public static final boolean Up = false;
	static Hashtable<KeyCode, Boolean> keys = new Hashtable<KeyCode, Boolean>();
	static {
		KeyboardFocusManager.getCurrentKeyboardFocusManager()
				.addKeyEventDispatcher(new Input());
	}
	private static HashSet<KeyCode> justchanged = new HashSet<>();

	public static boolean GetKey(KeyCode key) {
		Boolean ans = keys.get(key);
		if (ans == null)
			return false;
		return ans.booleanValue();
	}

	public static boolean GetKeyUp(KeyCode key) {
		return !GetKey(key) && justchanged.contains(key);
	}

	public static boolean GetKeyDown(KeyCode key) {
		return GetKey(key) && justchanged.contains(key);
	}

	private void KeyPressed(KeyCode key) {
		SetKey(key, Down);
	}

	private void KeyReleased(KeyCode key) {
		SetKey(key, Up);
	}

	private void SetKey(KeyCode key, boolean state) {
		Boolean oldState = keys.put(key, state);
		if (oldState == null || oldState.booleanValue() != state)
			justchanged.add(key);
	}

	public static void update() {
		ClearJustChanged();
	}

	public static void ClearJustChanged() {
		justchanged.clear();
	}

	public boolean dispatchKeyEvent(KeyEvent event) {
		KeyStroke ks = KeyStroke.getKeyStrokeForEvent(event);

		KeyCode key = KeyCode.GetKeyCodeFromVKCode(ks.getKeyCode());

		switch (ks.getKeyEventType()) {
		case KeyEvent.KEY_PRESSED:
			KeyPressed(key);
			break;
		case KeyEvent.KEY_RELEASED:
			KeyReleased(key);
			break;
		}
		return true;
	}
}
