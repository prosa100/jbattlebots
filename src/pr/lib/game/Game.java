package pr.lib.game;

import java.util.logging.Level;
import java.util.logging.Logger;
import pr.lib.Debug;

public abstract class Game extends Thread{

    protected Render r;

    public abstract void update();
    protected boolean running = false;

    public Game() {
        super();
    }
    private FreqLimiter fpsLimiter = new FreqLimiter(1000 / 30);

    public final void run() {
        Debug.log("Starting");
        onStart();
        Debug.log("Running");
        running = true;
        while (running) {
            if (Input.GetKey(KeyCode.Escape)) {
                quit();
                continue;
            }
            if (fpsLimiter.CanAndDo()) {
                update();
                Input.update();
                r.Draw();
            }
            pause(0);
        }
    }

    private void pause(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            Debug.error("Pause Interupted");
        }
    }

    final void quit() {
        running = false;
        Debug.log("Quitting");
        dispose();
        Debug.log("Exiting");
    }

    public abstract void onStart();

    public void dispose() {
        r.Dispose();
    }
}