package pr.lib.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import pr.lib.Debug;
import pr.lib.Dir;
import static pr.lib.Dir.North;
import static pr.lib.Dir.West;
import pr.lib.ImageLoader;

public class JGraphics {

    private JFrame frame;
    private Rectangle screenBounds;
    private BufferStrategy bufferStrategy;
    private Graphics2D g2 = null;
    private GraphicsDevice device;

    public void Init() {
        frame = new JFrame("Game");
        frame.setUndecorated(true);
        frame.setIgnoreRepaint(true);
        frame.setAlwaysOnTop(true);
        frame.setVisible(true);
        frame.createBufferStrategy(2);
        bufferStrategy = frame.getBufferStrategy();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        device = GraphicsEnvironment.getLocalGraphicsEnvironment()
                .getDefaultScreenDevice();
        //GraphicsConfiguration gc = device.getDefaultConfiguration();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        device.setFullScreenWindow(frame);
        ready = true;
    }
    private boolean ready = false;

    public void Dispose() {

        if (g2 != null) {
            g2.dispose();
        }
        bufferStrategy.dispose();
        bufferStrategy = null;
        frame.dispose();
        frame = null;
        ready = false;
    }

    public boolean startDraw() {
        //Debug.log("Start Draw");
        if (!ready) {
            Debug.error("JGraphics is not ready!");
            return false;
        }

        if (bufferStrategy.contentsLost()) {
            Debug.error("The bufferStrategy says its lost!");
            return false;
        }
        g2 = (Graphics2D) bufferStrategy.getDrawGraphics();
        screenBounds = frame.getBounds();
        g2.setFont(new Font("Dialog", Font.PLAIN, 72));

        g2.setColor(Color.BLACK);
        g2.fill(screenBounds);
        return true;
    }

    public boolean endDraw() {
        if (!ready || g2 == null) {
            Debug.error("Bad render state!");
            return false;
        }
        //Debug.log("End Draw");
        bufferStrategy.show();
        g2.dispose();
        return true;
    }

    public void drawText(String text, int x, int y) {
        g2.drawString(text, x, y);
    }

    public void fillRect(int x, int y, int width, int height) {
        g2.fillRect(x, y, width, height);
    }

    public void drawTexture(BufferedImage i, int x, int y) {
        g2.drawImage(i, null, x, y);
    }

    public void drawTexture(BufferedImage i, int x, int y, int w, int h, Dir d) {
        if (d == Dir.None) {
            drawTexture(i, x, y, w, h);
            return;
        }

        int imgWidth = i.getWidth();
        int imgHeight = i.getHeight();

        double scaleX = (double) w / imgWidth;
        double scaleY = (double) h / imgHeight;
        //r = 0;//Math.PI ;
        AffineTransform t = new AffineTransform();
        t.scale(scaleX, scaleY);
        t.quadrantRotate(d.value+1%4, imgWidth / 2, imgHeight / 2);
        //t.rotate(r, imgWidth / 2, imgHeight / 2);
        //t.rotate(r/180*Math.PI);



        double sy = y/ scaleY;
        double sx = x/scaleX;
        
        double tx = 0, ty =0;
        
        switch(d){
            case East:
                tx = sx ;
                ty = sy;
                break;
            case South:
                tx = sy;
                ty = -sx;
                break;
            case West:
                tx = -sx;
                ty = -sy;
                break;
            case North:
                tx = -sy;
                ty = sx;
                break;
            
        }
        
        t.translate(tx,ty);
        //t.translate(x/scaleX, y/scaleY);

        //Debug.log(t.getTranslateX() + "," + t.getTranslateY());

        g2.drawImage(i, t, frame);
        //AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, i.getWidth()/2, i.getHeight()/2);
    }

    public void drawTexture(BufferedImage t, int x, int y, int w, int h) {
        g2.drawImage(t, x, y, w, h, frame);
    }

    public Rectangle getScreen() {
        return screenBounds;
    }
}
