package pr.lib.game;

public class FreqLimiter {
	public final long minGapMS;

	
	private long last;
	public FreqLimiter(int minGapMS) {
		this.minGapMS = minGapMS;
		last = Time.nowMS() - minGapMS;
	}
	
	public boolean CanAndDo(){
		if(!Can())
			return false;
		Do();
		return true;
	}
	
	public void Do(){
		last = Time.nowMS();
	}
	
	public boolean Can(){
		return Time.nowMS()-last>minGapMS;
	}
}
