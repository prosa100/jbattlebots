package pr.lib.game;

public class Timer {
	long StartTime = 0;
	long StopTime = 0;

	public void Clear() {
		StartTime = 0;
		StopTime = 0;
	}

	public void Start() {
		Clear();
		StartTime = Time.nowMS();
	}

	public boolean HasStarted() {
		return StartTime != 0;
	}

	public boolean HasStopped() {
		return StopTime != 0;
	}

	public boolean IsRunning() {
		return HasStarted() && !HasStopped();
	}

	public long TimeElapsedMS() {
		return (HasStopped() ? StopTime : Time.nowMS()) - StartTime;
	}
	
	public double getTimeElapsedSeconds() {
		return TimeElapsedMS()/1000d;
	}

	public long Stop() {
		StopTime = Time.nowMS();
		return TimeElapsedMS();
	}
}
