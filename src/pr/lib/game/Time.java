package pr.lib.game;

import pr.lib.U;

public final class Time {
    public static final double Second = 1d;
    public static final double MilliSecond = U.Milli * Second;
    public static final double StartTime = now();
	public static long nowMS(){
		return System.currentTimeMillis();
	}
        /***
         * 
         * @return The current system time, in seconds from the epoch.
         */
	public static double now(){
            return nowMS()*MilliSecond;
        }
        public static double runtime(){
            return now()-StartTime;
        }
}
