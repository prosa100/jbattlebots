package pr.lib.game;

import java.awt.Rectangle;

import pr.battlebots.Thing;
import pr.battlebots.World;
import pr.lib.U;

public final class Render {

    protected final World world;
    protected final int cellSize;

    public Render(World world) {
        this.world = world;
        cellSize = world.cellSize;
        device = new JGraphics();
        device.Init();
        // new BufferedImage(world.width*world.cellSize,
        // world.height*world.cellSize, BufferedImage.TYPE_INT_ARGB);
    }
    private final JGraphics device;
    int lastXOffset = 0;
    int lastYOffset = 0;

    public void Draw() {
        device.startDraw();

        final int centerRow = world.getFocus().y;
        final int centerCol = world.getFocus().x;
        final Rectangle screen = device.getScreen();
        final int offsetX = (int) U.lerp(0.1, lastXOffset, screen.width / 2
                - centerCol * cellSize);
        final int offsetY = (int) U.lerp(0.1, lastYOffset, screen.height / 2
                - centerRow * cellSize);

        lastXOffset = offsetX;
        lastYOffset = offsetY;

        // Draw the background
        for (int y = offsetY % world.cellSize - world.cellSize; y < screen.height; y += world.cellSize) {
            for (int x = offsetX % world.cellSize - world.cellSize; x < screen.width; x += world.cellSize) {
                device.drawTexture(world.bgImage, x, y, world.cellSize,
                        world.cellSize);
            }
        }

        // Draw the foreground
        for (Thing t : world.getThingsToDraw()) {
            if (t.p == null) {
                continue;
            }
            device.drawTexture(t.getTexture(), t.p.x * world.cellSize
                    + offsetX, t.p.y * world.cellSize + offsetY,
                    world.cellSize, world.cellSize, t.d);
        }

        /*
         * int y = offsetY; for (int row = 0; row < world.height; row++) {
         * 
         * y += cellSize; if (!U.between(y, -world.cellSize, screen.height))
         * continue; int x = offsetX; for (int col = 0; col < world.height;
         * col++) { x += cellSize; if (!U.between(x, -world.cellSize,
         * screen.width)) continue;
         * 
         * Block b = world.GetBlock(new Pos(row, col)); BufferedImage i; if (b
         * == null) { i = world.bgImage; } else { i = b.GetTexture(); }
         * device.DrawTexture(i, x, y, world.cellSize, world.cellSize);
         * 
         * } }
         */

        device.drawText(world.getMessage(), screen.width / 2, 100);

        device.endDraw();
    }

    public void Dispose() {
        device.Dispose();
    }
}
