package pr.lib;

/**
 * @author prosa Some Utils
 */
public final class U {

    public static double Milli = 1 / 1000d;

    public static double invLerp(double v, double from, double to) {
        return (v - from) / (to - from);
    }

    public static double lerp(double v, double from, double to) {
        return (to - from) * v + from;
    }

    /**
     * Converts a n
     *
     * @param p A number 0 to 1 inclusive
     * @return A byte 0-255 representing that number
     */
    public static byte pToByte(double p) {
        return (byte) (p * 0xFF);
    }

    public static String loadTextFile(String path) {
        return "";
    }

    public static double clamp(double v, double min, double max) {
        if (v > max) {
            return max;
        }
        if (v < min) {
            return min;
        }
        return v;
    }

    public static boolean between(double v, double min, double max) {
        return v > min && v < max;
    }

    /**
     *
     * @param v
     * @param min Inclusive
     * @param max Exclusive
     * @return
     */
    public static boolean between(int v, int min, int max) {
        return v >= min && v < max;
    }

    public static int mod(int a, int b) {
        int x = a % b;
        if (x < 0) {
            x += b;
        }
        return x;
    }

    public static void copy(Object[] from, Object[] to) {
        System.arraycopy(from, 0, to, 0, from.length);
    }

    public static String join(int[] things, String delimiter) {
        String[] strs=new String[things.length];
        for (int i = 0; i < things.length; i++) {
            strs[i]=things[i]+"";
        }
        return join(strs,delimiter);
    }
    
    public static String join(float[] things, String delimiter) {
        String[] strs=new String[things.length];
        for (int i = 0; i < things.length; i++) {
            strs[i]=things[i]+"";
        }
        return join(strs,delimiter);
    }
    
    public static String join(String[] strs, String delimiter) {
        int length = (strs.length-1)*delimiter.length();
        for(String str : strs)
            length+=str.length();
        StringBuilder sb = new StringBuilder();
        
        for (Object thing : strs) {
            sb.append(thing);
            sb.append(delimiter);
        }
        return sb.toString();
    }

    public static float[] scale(int[] a, float s) {
        float[] o = new float[a.length];
        for (int i = 0; i < a.length; i++) {
            o[i] = a[i] * s;
        }
        return o;
    }

    public static float[] pnorm(int[] a) {
        return scale(a, 1.0F / sum(a));
    }

    public static int sum(int[] a) {
        int s = 0;
        for (int v : a) {
            s += v;
        }
        return s;
    }

    public static float[] accumulate(float[] a) {
        float[] o = new float[a.length];
        o[0] = a[0];
        for (int i = 1; i < a.length; i++) {
            o[i] = a[i] + o[i - 1];
        }
        return o;
    }
}
