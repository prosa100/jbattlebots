package pr.lib;

/**
 *
 * @author Paul
 */
public enum Turn {
    No(0),
    Right(1),
    Back(2),
    Left(3);
    
    protected final int value;
    Turn(int v){
        value = v;
    }
}
