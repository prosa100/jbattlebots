package pr.lib;


import java.io.PrintStream;
import pr.lib.game.Time;

/*
 * Use this to print things.
 */
public class Debug {
	private static PrintStream e = System.err;
	private static PrintStream o = System.out;
	public static void error(String msg){
		e.println(String.format("[%.2f]\t%s",Time.runtime(),msg));
	}
	public static void error(Object o){
            error(o.toString());
        }
        public static void log(String msg){
		o.println(String.format("[%.2f]\t%s",Time.runtime(),msg));
	}
        public static void log(Object o){
            log(o.toString());
        }
}
