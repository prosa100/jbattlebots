package pr.lib;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class ImageLoader {

    private static HashMap<String, BufferedImage> cashe = new HashMap<>();
    private static ClassLoader cl = textures.R.class.getClassLoader();

    public static BufferedImage Get(String name) {
        name = name.toLowerCase();
        BufferedImage i = cashe.get(name);
        if (i == null) {

            String s = name + ".png";
            
            URL u = textures.R.class.getResource(s);
            i = Load(u);
            if (i != null) {
                cashe.put(name, i);
                pr.lib.Debug.log("Loaded ["+name+"]");
            }
            else
            {
                Debug.error("Failed to load [" + name + "]");
            }
        }
        return i;
    }

    private static BufferedImage Load(URL path) {
        if(path == null)
            return null;
        try {
            return ImageIO.read(path);
        } catch (IOException e) {
            
        };
        return null;
    }
}
