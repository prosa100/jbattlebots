package pr.battlebots;

import pr.lib.Dir;

public class MazeRunner extends Mob {

    final IMazeRunnerAI ai;

    @Override
    public String getFullTypeName() {
        return "mobs/player";
    }

    public MazeRunner(World world, IMazeRunnerAI ai) {
        super(world.midpoint, world);
        this.ai = ai;
    }

    public boolean hasWon() {
        return p.equals(world.getGoal());
    }

    public int getMoves() {
        return numMoves;
    }
    
    int numMoves = 0;

    
    @Override
    public void update() {
        boolean[] open = new boolean[4];
        for (int i = 0; i < 4; i++) {
            open[i] = world.isEmpty(p.InDirection(Dir.dirs[i]));
        }
        if(ai==null)
            return;
        d = ai.GetMove(p, d, open);
        if (d != Dir.None) {
            moveForward();
            numMoves++;
        }
    }
}
