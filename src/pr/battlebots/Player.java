package pr.battlebots;

import pr.lib.Vec;
import pr.lib.game.Input;
import pr.lib.game.KeyCode;

public class Player extends Mob {

    @Override
    public String getFullTypeName() {
        return "mobs/player";
    }

    public Player(Vec p, World world) {
        super(p, world);
    }

    @Override
    public void update() {
        if (Input.GetKey(KeyCode.I)) {
            build();
        }
        if (Input.GetKey(KeyCode.Space)) {
            dig();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            turnLeft();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            turnRight();
        }

        if (Input.GetKey(KeyCode.UpArrow)) {
            moveForward();
        }
        if (Input.GetKey(KeyCode.DownArrow)) {
            moveBackwards();
        }
    }
}
