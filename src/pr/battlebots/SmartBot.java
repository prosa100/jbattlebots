/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pr.battlebots;

import java.util.Random;
import pr.lib.Dir;
import pr.lib.Vec;

/**
 *
 * @author Paul
 */
public class SmartBot implements IMazeRunnerAI{
    Random rng = new Random();

    @Override
    public Dir GetMove(Vec p, Dir d, boolean[] open) {
        return Dir.dirs[rng.nextInt(4)];
    }
}
