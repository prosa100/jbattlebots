package pr.battlebots;

import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Iterator;

import pr.battlebots.blocks.Block;
import pr.battlebots.blocks.BlockType;
import pr.battlebots.gen.BoundingBox;
import pr.battlebots.gen.IGen;
import pr.lib.Dir;
//import pr.lib.Debug;
import pr.lib.ImageLoader;
import pr.lib.U;
import pr.lib.Vec;
import pr.lib.game.Game;
import pr.lib.game.Render;

public class World extends Game implements Iterable<Vec> {

    public final int cellSize = 16;
    public final int width = 33;
    public final int height = width;
    public final Vec midpoint = new Vec(width / 2, height / 2);
    private final HashMap<Vec, Thing> things = new HashMap<>();
    public final BufferedImage bgImage = ImageLoader.Get("blocks/stone");
    private Thing focus;

    public void focusOn(Thing t) {
        focus = t;
    }


    public Thing get(Vec p) {
        return things.get(p);

    }

    public Collection<Thing> getThingsToDraw() {
        return things.values();
    }

    public final boolean isInbounds(Vec p) {
        return true;
        //return U.between(p.y, 0, height) && U.between(p.x, 0, width);
    }
    
    public final boolean inMaze(Vec p) {
        return U.between(p.y, 0, height) && U.between(p.x, 0, width);
    }
    
    public final boolean isOccupied(Vec p) {
        return things.containsKey(p);
    }

    public final boolean isEmpty(Vec p) {
        return isInbounds(p) && !isOccupied(p);
    }

    public World() {
        r = new Render(this);
    }

    @Override
    public void onStart() {
        gen(new BoundingBox());
    }

    public Vec getFocus() {
        if (focus == null) {
            return midpoint;
        }
        return focus.p;
    }

    @Override
    public final void update() {
        for (Object t : thingsToUpdate.toArray()) {
            ((Thing) t).update();
        }
        onUpdate();
    }
    
    protected void onUpdate(){
        
    }
    HashSet<Thing> thingsToUpdate = new HashSet<>();

    public final void move(Thing t, Vec p) {
        if (t.p != null) {
            remove(t);
        }
        if (t.needsUpdate) {
            thingsToUpdate.add(t);
        }

        things.put(p, t);

    }

    public final Block makeBlock(BlockType t, Vec p) {
        if (isOccupied(p)) {
            return null;
        }
        return new Block(t, p, this);
    }

    public final void remove(Thing t) {
        things.remove(t.p);
        if (t.needsUpdate) {
            thingsToUpdate.remove(t);
        }
    }

    public void clear(){
        for (Object t : things.values().toArray()) {
            remove((Thing) t);
        }
    }
    
    public void gen(IGen gen) {
        gen.gen(this);
    }

    public Vec getGoal(){
        return midpoint.InDirection(Dir.North,width/2);
    }
    
    String message = "";
    public void setMessage(String value){
        message=value;
    }
    public String getMessage(){
        return message;
    }
    @Override
    public Iterator<Vec> iterator() {
        return new VecIterator(0, height-1, 0, width-1);
    }
}
