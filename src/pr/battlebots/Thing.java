package pr.battlebots;

import java.awt.image.BufferedImage;
import pr.battlebots.inventory.ItemStack;
import pr.lib.Dir;
import pr.lib.Vec;

public abstract class Thing {

    protected final World world;
    public final boolean needsUpdate = true;
    public Dir d = Dir.None;
    protected int health = -1;
    public final Team team;
    public Vec p;

    public abstract String getFullTypeName();

    public Thing(Vec p, World world) {
        this(null, p, world);
    }

    public Thing(Team team, Vec p, World world) {
        this.world = world;
        moveTo(p);
        this.team = team;
    }

    public void update() {
    }

    public boolean getHurt(int amount, Thing by) {
        if (isIndestrucable()) {
            return false;
        }
        health -= amount;
        if (health <= 0) {
            health = 0;
            die(by);
            return true;
        }
        return false;
    }

    public void killed(KillData kd) {
        
    }
    
    public ItemStack onDie(){
        return null;
    }

    public void die(Thing cause) {
        KillData kd = new  KillData(this, cause, onDie());
        cause.killed(kd);//:[
        destroy();
    }

    public boolean isIndestrucable() {
        return health == -1;
    }

    @Override
    public String toString() {
        return "[" + d + "@" + p.toString() + "]";
    }

    public final void destroy() {
        world.remove(this);
    }

    public final boolean moveTo(Vec np) {
        if (np.equals(p)) {
            return true;
        }

        if (world.get(np) != null) {
            return false;
        }
        world.move(this, np);
        p = np;

        return true;
    }

    public abstract BufferedImage getTexture();
}