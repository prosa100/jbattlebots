package pr.battlebots.gen;

import pr.battlebots.World;
import pr.battlebots.blocks.BlockType;
import pr.lib.Vec;

public class FixedBox implements IGen {

    public final int width, height;
    public final Vec southWestCorner;
    public final BlockType blockType;

    public FixedBox(BlockType blocktype, Vec southWestCorner, int width, int height) {
        this.blockType = blocktype;
        this.width = width;
        this.height = height;
        this.southWestCorner = southWestCorner;
    }

    @Override
    public void gen(World world) {
        for (int x = 0; x < width; x++) {
            world.makeBlock(blockType, southWestCorner.plus(new Vec(x, 0)));
            world.makeBlock(blockType, southWestCorner.plus(new Vec(x, width - 1)));
        }
        for (int y = 0; y < height; y++) {
            world.makeBlock(blockType, southWestCorner.plus(new Vec(0, y)));
            world.makeBlock(blockType, southWestCorner.plus(new Vec(height - 1, y)));
        }
    }
}
