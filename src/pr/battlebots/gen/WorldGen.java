package pr.battlebots.gen;

import java.util.Iterator;
import pr.battlebots.VecIterator;
import pr.battlebots.World;
import pr.battlebots.blocks.Block;
import pr.battlebots.blocks.BlockType;
import pr.lib.Vec;

public abstract class WorldGen implements IGen{

    protected World world;

    abstract BlockType getBlockFor(Vec p);

    abstract Vec getSouthWestCorner();

    abstract int getWidth();

    abstract int getHeight();

    void setWorld(World world) {
        this.world = world;
    }
    
    @Override
    public void gen(World world){
        this.setWorld(world);
        int startCol = this.getSouthWestCorner().x;
        int startRow = this.getSouthWestCorner().y;
        int lastRow = this.getWidth()+startRow;
        int lastCol = this.getHeight()+startCol;
        Iterator<Vec> iterator = new VecIterator(startRow, lastRow, startCol, lastCol);
        
        while(iterator.hasNext()) {
            Vec p = iterator.next();
            BlockType t = this.getBlockFor(p);

            if (t != null) {
                world.makeBlock(t, p);
            }
        }
    }
}
