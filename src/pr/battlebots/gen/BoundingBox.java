/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pr.battlebots.gen;

import pr.battlebots.World;
import pr.battlebots.blocks.BlockType;
import pr.lib.Vec;

/**
 *
 * @author Paul
 */
public class BoundingBox implements IGen{

    public final BlockType blockType = BlockType.BEDROCK;
    public void gen(World w) {
        for (int x = -1; x <= w.width; x++) {
            w.makeBlock(blockType, new Vec(x, -1));
            w.makeBlock(blockType, new Vec(x, w.width));
        }
        for (int y = -1; y <= w.height; y++) {
            w.makeBlock(blockType, new Vec(-1, y));
            w.makeBlock(blockType, new Vec(w.height, y));
        }
    }
}
