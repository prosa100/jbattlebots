package pr.battlebots;

import java.util.Iterator;
import pr.lib.Vec;

public class VecIterator implements Iterator<Vec> {

        public VecIterator(int rowA, int rowB, int colA, int colB) {
            this.startCol = Math.min(colA, colB);
            this.startRow = Math.min(rowA, rowB);
            this.endCol = Math.max(colA, colB);
            this.endRow = Math.max(rowA, rowB);
            this.row = rowA;
            this.col = colA;
        }
        public final int startRow, startCol, endRow, endCol;
        int row, col;

        @Override
        public boolean hasNext() {
            return col <= endCol && row <= endRow;
        }

        @Override
        public Vec next() {

            Vec v = new Vec(col, row);
            //Debug.log(v.toString());
            col++;
            if (col > endCol) {
                col = startCol;
                row++;
            }

            return v;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("I'm sorry Dave, I'm afraid I can't do that.");
        }
    }