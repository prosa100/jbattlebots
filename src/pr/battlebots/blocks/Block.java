package pr.battlebots.blocks;
import java.awt.image.BufferedImage;

import pr.battlebots.Thing;
import pr.battlebots.World;
import pr.lib.Vec;

public class Block extends Thing {
	final BlockType type;

	public String getFullTypeName(){
		return type.fullName;
	}
	
	public Block(BlockType type, Vec p, World world) {
		super(p,world);
		this.type = type;
		this.health=type.health;
	}
	
        @Override
	public BufferedImage getTexture(){
		return type.texture;
	}
}
