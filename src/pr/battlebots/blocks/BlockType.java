package pr.battlebots.blocks;


import java.awt.image.BufferedImage;

import pr.lib.ImageLoader;



public class BlockType {
	public final String name;
	public final BufferedImage texture;
	public final char id;
	
	public final String fullName;
	
	public final boolean isSolid = true;
	public final boolean isStatic = true;
	
	public final int health;
	
	protected static BlockType[] BlockTypes = new BlockType[512];
	
	protected BlockType(String fullName, String name, String textureName, int health, char id) {
		this.fullName = fullName;
		this.name = name;
		this.texture = ImageLoader.Get(textureName);
		this.id = id;
		this.health = health;
		BlockTypes[id]=this;
	}
	
	public BlockType getBlockType(char id){
		return BlockTypes[id];
	}
	public static final BlockType IRON = new BlockType("minecraft.block.iron","Iron Block","blocks/iron_block",20,'I');
	public static final BlockType BEDROCK = new BlockType("minecraft.block.bedrock","Bedrock","blocks/bedrock",-1,'0');
        public static final BlockType IRON_ORE = new BlockType("minecraft.block.iron.ore","Iron Ore","blocks/iron_ore",4,'i'); 
}
