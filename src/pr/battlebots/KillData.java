/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pr.battlebots;

import pr.battlebots.inventory.ItemStack;

/**
 *
 * @author Paul
 */
public class KillData {
    public final Thing victim;
    public final Thing killer;
    public final ItemStack droppings;
    public KillData(Thing victim, Thing killer, ItemStack droppings){
        this.victim=victim;
        this.killer=killer;
        this.droppings=droppings;
    }
}
