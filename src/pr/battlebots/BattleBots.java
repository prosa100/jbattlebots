package pr.battlebots;

import pr.battlebots.blocks.BlockType;
import pr.battlebots.gen.FixedBox;
import pr.battlebots.gen.MazeGen;
import pr.lib.Vec;
import pr.lib.game.Input;
import pr.lib.game.KeyCode;

public class BattleBots extends World {

    public static void main(String[] args) {

        new BattleBots().start();
    }
    Player p;

    @Override
    public void onStart() {
        super.onStart();
        //p=new Player(new Vec(width / 2, height / 2), this);
        //this.focusOn(p);
        //BBAM bbam = new BBAM(new Vec(4,4), this);
        //gen(new FixedBox(BlockType.IRON, new Vec(5,5), 3,3));

    }

    MazeRunner runner;
    boolean nextMap = true;

    private void next() {
        this.focusOn(null);
        clear();
        MazeGen mg = new MazeGen();
        mg.seed = (int) System.currentTimeMillis();
        gen(mg);
        runner = new MazeRunner(this, new SmartBot());
        this.focusOn(runner);
    }

    
    
    @Override
    protected void onUpdate() {
        if (runner == null) {
            if (Input.GetKeyDown(KeyCode.G)) {
                next();
            }
        } else {
            if (runner.hasWon()) {
                setMessage("Won in "+runner.getMoves());
                runner = null;
            }
        }
    }
}
