package pr.battlebots;

import java.awt.image.BufferedImage;
import pr.battlebots.blocks.BlockType;
import pr.battlebots.inventory.Inventory;
import pr.lib.Debug;
import pr.lib.Dir;
import pr.lib.ImageLoader;
import pr.lib.Turn;
import pr.lib.Vec;
import pr.lib.game.FreqLimiter;

public class Mob extends Thing {

    protected int attack = 1;
    protected final Inventory inv;
    private final FreqLimiter moveFreqLimiter = new FreqLimiter(80);
    private final FreqLimiter digFreqLimiter = new FreqLimiter(10);
    private final BufferedImage texture;

    @Override
    public BufferedImage getTexture() {
        return texture;
    }

    public Mob(Vec p, World world) {
        super(p, world);
        inv = new Inventory(16);

        texture = ImageLoader.Get(this.getFullTypeName());
        this.health = 10;
        d = Dir.East;
    }

    @Override
    public void update() {
    }

    public Thing thingInFront() {
        return world.get(posInFront());
    }

    public Vec posInFront() {
        return p.InDirection(d);
    }

    public void dig() {
        if (!digFreqLimiter.Can()) {
            return;
        }

        Thing inFront = thingInFront();
        if (inFront == null) {
            return;
        }
        inFront.getHurt(attack, this);
        digFreqLimiter.Do();
    }

    public void killed(Thing t){
    }
    
    public boolean move(Dir d) {
        if (!moveFreqLimiter.Can()) {
            return false;
        }
        Vec tp = p.InDirection(d);
        if (world.get(tp) == null) {
            boolean moveSucceeded = moveTo(p.InDirection(d));
            if (moveSucceeded) {
                moveFreqLimiter.Do();
            }
            return moveSucceeded;
        }
        return false;
    }

    public boolean turn(Turn t) {
        d = d.turn(t);

        return true;
    }

    public boolean moveForward() {
        return move(d);
    }

    public boolean moveBackwards() {
        return move(d.Back);
    }

    public boolean build() {
        Vec tp = posInFront();
        if (world.get(tp) != null) {
            return false;
        }
        world.makeBlock(BlockType.IRON, tp);
        return true;
    }

    public boolean turnRight() {
        return turn(Turn.Right);
    }

    public boolean turnLeft() {
        return turn(Turn.Left);
    }

    @Override
    public String getFullTypeName() {
        return "mobs/mob";
    }
}
